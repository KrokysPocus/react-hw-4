const TOGGLE_FAVORITE_PRODUCT = 'TOGGLE_FAVORITE_PRODUCT';

const favoriteFromStorage = JSON.parse(localStorage.getItem('favoriteProductsId'));

const initialState = {
    favoritesInCart: favoriteFromStorage || []
};

const favoritesReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_FAVORITE_PRODUCT:
            return {
                ...state,
                favoritesInCart: action.payload
            };
        default:
            return state;
    }
};

export const toggleFavoritesAC = (productId) => ({
    type: TOGGLE_FAVORITE_PRODUCT,
    payload: productId
});
export default favoritesReducer;
